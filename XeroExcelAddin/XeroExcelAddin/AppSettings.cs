﻿namespace XeroExcelAddin
{
    public static class AppSettings
    {
        public static string BaseXeroUrl => System.Configuration.ConfigurationManager.AppSettings.Get("baseXeroUrl");
        public static string ConsumerKey => System.Configuration.ConfigurationManager.AppSettings.Get("consumerKey");
        public static string ConsumerSecret => System.Configuration.ConfigurationManager.AppSettings.Get("consumerSecret");
        public static string Organisation => "Organisation";
        public static string AgedPayables => "AgedPayables";
        public static string ProfitAndLoss => "ProfitAndLoss";
        public static string TrialBalance => "TrialBalance";
        public static string BankSummary => "BankSummary";
        public static string BudgetSummary => "BudgetSummary";
        public static string ExecutiveSummary => "ExecutiveSummary";
        public static string BankStatement => "BankStatement";
        public static string Receivables => "Receivables";
        public static string Invoices => "Invoices";
        public static string Contacts => "Contacts";
        public static string Payments => "Payments";
        public static string BalanceSheet => "BalanceSheet";
        public static string AgedReceivables => "AgedReceivables";
        public static string CurrentOrganisation => "CurrentOrganisation";
    }
}
