﻿using System;
using Castle.DynamicProxy;
using Xero.Api.Infrastructure.OAuth;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.Infrastructure
{
    public class LoggingAspect : IInterceptor
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void Intercept(IInvocation invocation)
        {
            //        var listOfArgumumentValues = new List<string>();
            //        var argsObj = invocation.Arguments.Where(y => y is ILoginMarker).Select(x => x).ToList();
            //        argsObj.ForEach(obj => listOfArgumumentValues.AddRange(obj.GetPropValues()));
            //        listOfArgumumentValues.AddRange(invocation.Arguments.Where(y => y is string).Select(a => (a ?? string.Empty).ToString()).ToArray());
            //        listOfArgumumentValues.AddRange(invocation.Arguments.Where(y => y.GetType().IsValueType).Select(a => (a ?? string.Empty).ToString()).ToArray());
            //        log.Debug($"{invocation.Method.Name} method invoked with parameters : {string.Join(",", listOfArgumumentValues)}");
            try
            {
                invocation.Proceed();
            }
            catch (OAuthException e)
            {
                Info.Create("Xero connection error - the code that you provided is not valid please try again", "Xero oAuth error");
                log.Error(e);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }
    }
}
