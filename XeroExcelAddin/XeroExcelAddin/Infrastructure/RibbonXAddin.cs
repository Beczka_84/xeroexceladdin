﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Autofac;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Xero.Api.Infrastructure.Interfaces;
using XeroExcelAddin.Properties;
using XeroExcelAddin.Services;
using XeroExcelAddin.ViewModels;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.Infrastructure
{
    [ComVisible(true)]
    public class RibbonXAddin : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;
        private readonly string InfoMessage = "\n\n Please wait \n\n ";
        private Application olApplication;

        public RibbonXAddin(Application olApplication)
        {
            this.olApplication = olApplication;
        }

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            ThisAddIn.m_Ribbon = ribbonUI;
        }

        public string GetCustomUI(string RibbonID)
        {
            string customUI = string.Empty;
            switch (RibbonID)
            {
                case "Microsoft.Excel.Workbook":
                    customUI = GetResourceText(
                        "XeroExcelAddin.Resources.Explorer.xml");
                    return customUI;
                default:
                    return string.Empty;
            }
        }

        public bool Login_Enabled(Office.IRibbonControl control)
        {
            var result = Globals.ThisAddIn.Core.Resolve<ICacheService>().Get<IToken>(Environment.MachineName);
            return result == null;
        }

        public void Login_Click(Office.IRibbonControl control)
        {
            var orgName = Globals.ThisAddIn.Core.Resolve<IXeroService>().Login();
        }
        public Bitmap GetImage(Office.IRibbonControl control)
        {
            return new Bitmap(Resources.DashboardDivaLogoNew);
        }

        public bool Logout_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public string GetDropDownLabel(Office.IRibbonControl control)
        {
            if (IsUserLogin())
            {
                var orgName = Globals.ThisAddIn.Core.Resolve<IXeroService>().Login();
                if (string.IsNullOrEmpty(orgName))
                {
                    return "Please login to xero first";
                }
                return $"Available Actions for {orgName}";
            }
            return "Please login to xero first";
        }

        public bool DropDown_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void Logout_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Core.Resolve<ICacheService>().Set<IToken>(Environment.MachineName, null);
            Globals.ThisAddIn.Core.Resolve<IRibbonService>().ReserLoginButtons();
        }

        public bool Organisation_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void Organisation_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Core.Resolve<IXeroService>().GetOrganisation();
        }

        public bool ReportAgedPayables_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void ReportAgedPayables_Click(Office.IRibbonControl control)
        {
            var context = new ContactViewModel();
            var dialog = Contact.ShowDialog(context);
            if (dialog != null)
            {
                if (!string.IsNullOrEmpty(context.ContactId))
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetAgedPayables(context.SelectedContactId, context.Date);
                }
                else
                {
                    Info.Create("\n\n No contact selected - please try again \n\n", "Xero error");
                }
            }
        }
        public bool Contacts_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void Contacts_Click(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Core.Resolve<IXeroService>().GetContacts();
        }

        public bool ReportAgedReceivables_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void ReportAgedReceivables_Click(Office.IRibbonControl control)
        {
            var context = new ContactViewModel();
            var dialog = Contact.ShowDialog(context);
            if (dialog != null)
            {
                if (!string.IsNullOrEmpty(context.ContactId))
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetReceivables(context.SelectedContactId, context.Date);
                }
                else
                {
                    Info.Create("\n\n No contact selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool BankSummary_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void BudgetSummary_Click(Office.IRibbonControl control)
        {
            var context = new DateSelectionViewModel();
            var dialog = DateSelection.ShowDialog(context);
            if (dialog != null)
            {
                if (context.Date.HasValue)
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetBudgetSummary(context.Date.Value);
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }

        }

        public bool BudgetSummary_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void ExecutiveSummary_Click(Office.IRibbonControl control)
        {
            var context = new DateSelectionViewModel();
            var dialog = DateSelection.ShowDialog(context);
            if (dialog != null)
            {
                if (context.Date.HasValue)
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetExecutiveSummary(context.Date.Value);
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool ExecutiveSummary_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void BankSummary_Click(Office.IRibbonControl control)
        {
            var context = new BankViewModel();
            var dialog = BankSummary.ShowDialog(context);
            if (dialog != null)
            {
                if (context.From.HasValue && context.To.HasValue)
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetBankSummary(context.From.Value, context.To.Value);
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool BankStatement_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public void BankStatement_Click(Office.IRibbonControl control)
        {
            var context = new BankStatementViewModel();
            var dialog = BankStatement.ShowDialog(context);
            if (dialog != null)
            {
                if (context.From.HasValue && context.To.HasValue && !string.IsNullOrEmpty(context.AccountId))
                {
                    Globals.ThisAddIn.Core.Resolve<IXeroService>().GetBankStatement(context.AccountId, context.From.Value, context.To.Value);
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool TrialBalance_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public async void TrialBalance_Click(Office.IRibbonControl control)
        {
            var context = new DateSelectionViewModel();
            var dialog = DateSelection.ShowDialog(context);
            if (dialog != null)
            {
                if (context.Date.HasValue)
                {
                    var instance = new PlaseWait();
                    instance.Show();
                    await Task.Run(() =>
                    {
                        Globals.ThisAddIn.Core.Resolve<IXeroService>().GetTrialBalance(context.Date.Value);
                    });
                    instance.CloseScreen();
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool ProfitAndLoss_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public async void ProfitAndLoss_Click(Office.IRibbonControl control)
        {
            var context = new DateSelectionViewModel();
            var dialog = DateSelection.ShowDialog(context);
            if (dialog != null)
            {
                if (context.Date.HasValue)
                {
                    var instance = new PlaseWait();
                    instance.Show();
                    await Task.Run(() =>
                    {
                        Globals.ThisAddIn.Core.Resolve<IXeroService>().GetProfitAndLoss(context.Date.Value);
                    });
                    instance.CloseScreen();
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool ReportBalanceSheet_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public async void ReportBalanceSheet_Click(Office.IRibbonControl control)
        {
            var context = new DateSelectionViewModel();
            var dialog = DateSelection.ShowDialog(context);
            if (dialog != null)
            {
                if (context.Date.HasValue)
                {
                    var instance = new PlaseWait();
                    instance.Show();
                    await Task.Run(() =>
                    {
                        Globals.ThisAddIn.Core.Resolve<IXeroService>().GetBalanceSheet(context.Date.Value);
                    });
                    instance.CloseScreen();
                }
                else
                {
                    Info.Create("\n\n No date selected - please try again \n\n", "Xero error");
                }
            }
        }

        public bool Invoices_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public async void Invoices_Click(Office.IRibbonControl control)
        {
            var instance = new PlaseWait();
            instance.Show();
            await Task.Run(() =>
            {
                Globals.ThisAddIn.Core.Resolve<IXeroService>().GetInvoices();
            });
            instance.CloseScreen();
        }

        public bool Payments_Enabled(Office.IRibbonControl control)
        {
            return IsUserLogin();
        }

        public async void Payments_Click(Office.IRibbonControl control)
        {
            var instance = new PlaseWait();
            instance.Show();
            await Task.Run(() =>
            {
                Globals.ThisAddIn.Core.Resolve<IXeroService>().GetPayments();
            });
            instance.CloseScreen();
        }

        #region Helpers

        private bool IsUserLogin()
        {
            var result = Globals.ThisAddIn.Core.Resolve<ICacheService>().Get<IToken>(Environment.MachineName);
            return result != null;
        }

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (
                        StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}