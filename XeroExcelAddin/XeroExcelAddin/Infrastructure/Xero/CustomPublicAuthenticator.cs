﻿using System;
using System.Reflection;
using Xero.Api.Example.Applications.Public;
using Xero.Api.Infrastructure.Interfaces;
using XeroExcelAddin.ViewModels;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.Infrastructure.Xero
{
    public class CustomPublicAuthenticator : PublicAuthenticator
    {
        public CustomPublicAuthenticator(string baseUri, string tokenUri, string callBackUrl, ITokenStore store,
            string scope = null) : base(baseUri, tokenUri, callBackUrl, store, scope)
        {
        }

        protected override string AuthorizeUser(IToken token)
        {
            var code = base.AuthorizeUser(token);
            if (string.IsNullOrEmpty(code))
            {
                var context = new CodeAuthenticationViewModel();
                var dialog = CodeAuthentication.ShowDialog(context);
                if (dialog != null)
                {
                    if (string.IsNullOrEmpty(context.Code))
                    {
                        return code;
                    }
                    return context.Code;
                }
            }
            return code;
        }
    }
}