﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autofac;
using XeroExcelAddin.Services;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for Contact.xaml
    /// </summary>
    public partial class Contact : Window
    {
        private static Contact _inst = null;
        public Contact(ContactViewModel context)
        {
            InitializeComponent();
            this.DataContext = context;
        }

        public static bool? ShowDialog(ContactViewModel datacontext)
        {
            _inst = new Contact(datacontext);
            return _inst.ShowDialog();
        }

        public static Contact GetInstance()
        {
            return _inst;
        }

        private void Contact_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }
    }
}
