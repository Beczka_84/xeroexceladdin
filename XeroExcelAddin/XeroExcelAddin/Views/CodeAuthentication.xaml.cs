﻿using System.ComponentModel;
using System.Windows;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for CodeAuthentication.xaml
    /// </summary>
    public partial class CodeAuthentication : Window
    {
        private static CodeAuthentication _inst = null;

        public CodeAuthentication(CodeAuthenticationViewModel datacontext)
        {
            InitializeComponent();
            this.DataContext = datacontext;
        }

        public static bool? ShowDialog(CodeAuthenticationViewModel datacontext)
        {
            _inst = new CodeAuthentication(datacontext);
            return _inst.ShowDialog();
        }

        private void CodeAuthentication_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }

        public static CodeAuthentication GetInstance()
        {
            return _inst;
        }
    }
}
