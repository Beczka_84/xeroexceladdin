﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for PlaseWait.xaml
    /// </summary>
    public partial class PlaseWait : Window
    {
        public PlaseWait()
        {
            InitializeComponent();
        }

        public void CloseScreen()
        {
            this.Dispatcher.Invoke(this.Close);
        }
    }
}
