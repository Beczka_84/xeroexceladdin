﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for BankSummary.xaml
    /// </summary>
    public partial class BankSummary : Window
    {
        private static BankSummary _inst = null;
        public BankSummary(BankViewModel context)
        {
            InitializeComponent();
            this.DataContext = context;
        }

        public static bool? ShowDialog(BankViewModel datacontext)
        {
            _inst = new BankSummary(datacontext);
            return _inst.ShowDialog();
        }

        public static BankSummary GetInstance()
        {
            return _inst;
        }

        private void Bank_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }

    }
}
