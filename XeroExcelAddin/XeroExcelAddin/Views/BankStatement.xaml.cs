﻿using System.ComponentModel;
using System.Windows;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for BankStatement.xaml
    /// </summary>
    public partial class BankStatement : Window
    {
        public BankStatement()
        {
            InitializeComponent();
        }

        private static BankStatement _inst = null;
        public BankStatement(BankStatementViewModel context)
        {
            InitializeComponent();
            this.DataContext = context;
        }

        public static bool? ShowDialog(BankStatementViewModel datacontext)
        {
            _inst = new BankStatement(datacontext);
            return _inst.ShowDialog();
        }

        public static BankStatement GetInstance()
        {
            return _inst;
        }

        private void BankStatement_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }
    }
}
