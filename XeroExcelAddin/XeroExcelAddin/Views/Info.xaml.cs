﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for Info.xaml
    /// </summary>
    public partial class Info : Window
    {
        public Info()
        {
            InitializeComponent();
        }

        private static Info _inst = null;

        public static void Create(string message, string title)
        {
            if (_inst == null)
            {
                _inst = new Info(message, title);
                _inst.Show();
                _inst.UpdateLayout();

            }
            else
            {
                _inst.Activate();
            }
        }

        public static Info GetInstance()
        {
            return _inst;
        }
        public static void CloseInstance()
        {
            _inst.Close();
            _inst = null;
        }

        public Info(string message, string title)
        {
            InitializeComponent();
            this.DataContext = new InfoViewModel(message, title);
            Dispatcher.Invoke(new Action(() => { }), DispatcherPriority.ContextIdle, null);
        }

        private void Info_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }
    }
}
