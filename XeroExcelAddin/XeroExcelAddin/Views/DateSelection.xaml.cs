﻿using System.ComponentModel;
using System.Windows;
using XeroExcelAddin.ViewModels;

namespace XeroExcelAddin.Views
{
    /// <summary>
    /// Interaction logic for DateSelection.xaml
    /// </summary>
    public partial class DateSelection : Window
    {
        private static DateSelection _inst = null;
        
        public DateSelection(DateSelectionViewModel context)
        {
            InitializeComponent();
            this.DataContext = context;
        }

        public static bool? ShowDialog(DateSelectionViewModel datacontext)
        {
            _inst = new DateSelection(datacontext);
            return _inst.ShowDialog();
        }

        public static DateSelection GetInstance()
        {
            return _inst;
        }

        public static void ClearInstance()
        {
            _inst.Close();
            _inst = null;
        }

        private void DateSelection_OnClosing(object sender, CancelEventArgs e)
        {
            _inst = null;
        }
    }
}
