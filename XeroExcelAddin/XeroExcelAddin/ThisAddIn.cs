﻿using Autofac;
using Office = Microsoft.Office.Core;
using XeroExcelAddin.Infrastructure;

namespace XeroExcelAddin
{
    public partial class ThisAddIn
    {
        private IContainer _core;
        internal static Office.IRibbonUI m_Ribbon;

        public IContainer Core => _core;
        public log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //log4net config
            log4net.Config.XmlConfigurator.Configure();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new RibbonXAddin(this.Application);
        }

        public override void BeginInit()
        {
            _core = AddInBootstrapper.InitContainer();
            MapperModule.Initialize();
            base.BeginInit();
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
