﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Reports;
using Xero.Api.Core.Model.Types;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using XeroExcelAddin.Infrastructure.Xero;
using XeroExcelAddin.Views;
using BaseService = XeroExcelAddin.Services.Base.BaseService;
using Contact = Xero.Api.Core.Model.Contact;

namespace XeroExcelAddin.Services
{
    public interface IXeroService
    {
        string Login();
        void GetOrganisation();
        void GetAgedPayables(Xero.Api.Core.Model.Contact contact, DateTime? period);
        void GetReceivables(Xero.Api.Core.Model.Contact contact, DateTime? period);
        void GetBalanceSheet(DateTime period);
        void GetTrialBalance(DateTime period);
        void GetProfitAndLoss(DateTime period);
        void GetBankSummary(DateTime? from, DateTime? to);
        void GetBudgetSummary(DateTime period);
        void GetExecutiveSummary(DateTime period);
        void GetBankStatement(string accountId, DateTime? from, DateTime? to);
        void GetInvoices();
        void GetPayments();
        void GetContacts();
        IList<Account> GetBankAccounts();
        IEnumerable<Contact> GetAvialableContacts();
    }

    public class XeroService : BaseService, IXeroService
    {
        private readonly ITokenStore _tokenStore;
        private readonly IRibbonService ribbonService;
        private readonly IExcelApiService excelApiService;
        private readonly ICacheService cacheService;
        private readonly XeroCoreApi _xeroCoreApi;
      
        public XeroService(ITokenStore tokenStore, IRibbonService ribbonService, IExcelApiService excelApiService, ICacheService cacheService)
        {
            this._tokenStore = tokenStore;
            this.ribbonService = ribbonService;
            this.excelApiService = excelApiService;
            this.cacheService = cacheService;
            this._xeroCoreApi = CreateServiceWrapper();
        }

        public string Login()
        {
            var result = _xeroCoreApi.Organisation;
            ribbonService.ReserLoginButtons();
            cacheService.Set(AppSettings.CurrentOrganisation, result);
            return result.Name;
        }

        //Aged Payables - done
        //Aged Receivables - done
        //Balance Sheet - done
        //Bank Statement -done
        //Bank Summary - done
        //Budget Summary - done
        //Executive Summary - done
        //Profit and Loss - done
        //Trial Balance - done

        public void GetOrganisation()
        {
            var result = _xeroCoreApi.Organisation;
            excelApiService.AddExcelWorkSheet(AppSettings.Organisation);
            excelApiService.AddOrganisationToWorkSheet(result, AppSettings.Organisation);
        }

        public void GetAgedPayables(Xero.Api.Core.Model.Contact contact, DateTime? period)
        {
            var result = _xeroCoreApi.Reports.AgedPayables(contact.Id, period);
            excelApiService.AddExcelWorkSheet(AppSettings.AgedPayables);
            excelApiService.AddAgedPayablesToWorkSheet(result, AppSettings.AgedPayables, contact.Name);
        }

        public void GetReceivables(Xero.Api.Core.Model.Contact contact, DateTime? period)
        {
            var result = _xeroCoreApi.Reports.AgedReceivables(contact.Id, period);
            excelApiService.AddExcelWorkSheet(AppSettings.AgedReceivables);
            excelApiService.AddAgedReceivablesToWorkSheet(result, AppSettings.AgedReceivables, contact.Name);
        }

        public void GetBalanceSheet(DateTime period)
        {
            var result = _xeroCoreApi.Reports.BalanceSheet(period);
            var dict = new Dictionary<string, Dictionary<string, string>>();
            for (int i = 1; i < 12; i++)
            {
                var date = period.AddMonths(-i);
                string elementsToAdd = "";
                var prevMonthReport = _xeroCoreApi.Reports.BalanceSheet(date);
                var firstOrDefault = prevMonthReport.Rows.FirstOrDefault(x => x.RowType == "Header");
                if (firstOrDefault != null)
                {
                    elementsToAdd = firstOrDefault.Cells[1].Value;
                }

                var prevMonthalues = prevMonthReport.Rows
                    .Where(x => x.Rows != null)
                    .SelectMany(x => x.Rows)
                    .Where(x => x.Cells[0] != null && x.Cells[1] != null)
                    .Select(x => new
                    {
                        Key = x.Cells[0].Value,
                        Value = x.Cells[1].Value
                    }).ToDictionary(x => x.Key, y => y.Value);

                dict.Add(elementsToAdd, prevMonthalues);
            }
            excelApiService.AddExcelWorkSheet(AppSettings.BalanceSheet);
            excelApiService.AddBalanceSheetToWorkSheet(result, AppSettings.BalanceSheet, dict);
        }

        public void GetTrialBalance(DateTime period)
        {
            var _lastDay = DateTime.DaysInMonth(period.Year, period.Month);
            var _dateTo = new DateTime(period.Year, period.Month, _lastDay);

            var result = _xeroCoreApi.Reports.TrailBalance(_dateTo);
            var dict = new Dictionary<string, Dictionary<string, string>>();
            for (int i = 0; i < 12; i++)
            {
                var date = _dateTo.AddMonths(-i);
                string elementsToAdd = "";
                var prevMonthReport = _xeroCoreApi.Reports.TrailBalance(date);
                var firstOrDefault = prevMonthReport.Rows.FirstOrDefault(x => x.RowType == "Header");
                if (firstOrDefault != null)
                {
                    elementsToAdd = date.ToShortDateString();
                }

                var prevMonthalues = prevMonthReport.Rows
                    .Where(x => x.Rows != null)
                    .SelectMany(x => x.Rows)
                    .Where(x => x.Cells != null && x.Cells[0] != null && (x.Cells[1] != null || x.Cells[2] != null))
                    .Select(x => new
                    {
                        Key = x.Cells[0].Value,
                        Value = CheckDebitOrCreditValue(x)
                    }).ToDictionary(x => x.Key, y => y.Value);

                dict.Add(elementsToAdd, prevMonthalues);
            }
            excelApiService.AddExcelWorkSheet(AppSettings.TrialBalance);
            excelApiService.AddTrialBalanceToWorkSheet(result, AppSettings.TrialBalance, dict);
        }

        private string CheckDebitOrCreditValue(ReportRow reportRow)
        {
            if (string.IsNullOrEmpty(reportRow.Cells[1].Value))
            {
                //Credit
                return $"-{reportRow.Cells[2].Value}";
            }
            //Debit
            return reportRow.Cells[1].Value;
        }

        public void GetProfitAndLoss(DateTime period)
        {
            var _lastDay = DateTime.DaysInMonth(period.Year, period.Month);
            var _from = new DateTime(period.Year, period.Month, 1);
            var _to = new DateTime(period.Year, period.Month, _lastDay);

            var result = _xeroCoreApi.Reports.ProfitAndLoss(period, _from, _to);
            var dict = new Dictionary<string, Dictionary<string, string>>();
            for (int i = 1; i < 12; i++)
            {
                var date = period.AddMonths(-i);
                string elementsToAdd = "";
                var lastDay = DateTime.DaysInMonth(date.Year, date.Month);
                var from = new DateTime(date.Year, date.Month, 1);
                var to = new DateTime(date.Year, date.Month, lastDay);
                var prevMonthReport = _xeroCoreApi.Reports.ProfitAndLoss(date, from, to);
                var firstOrDefault = prevMonthReport.Rows.FirstOrDefault(x => x.RowType == "Header");
                if (firstOrDefault != null)
                {
                    elementsToAdd = firstOrDefault.Cells[1].Value;
                }

                var prevMonthalues = prevMonthReport.Rows
                    .Where(x => x.Rows != null)
                    .SelectMany(x => x.Rows)
                    .Where(x => x.Cells != null && x.Cells.Count == 2 && x.Cells[0] != null && x.Cells[1] != null)
                    .Select(x => new
                    {
                        Key = x.Cells[0].Value,
                        Value = x.Cells[1].Value
                    }).ToDictionary(x => x.Key, y => y.Value);

                dict.Add(elementsToAdd, prevMonthalues);
            }
            excelApiService.AddExcelWorkSheet(AppSettings.ProfitAndLoss);
            excelApiService.AddProfitAndLossToWorkSheet(result, AppSettings.ProfitAndLoss, dict);
        }

        public void GetBankSummary(DateTime? from, DateTime? to)
        {
            var result = _xeroCoreApi.Reports.BankSummary(from, to);
            excelApiService.AddExcelWorkSheet(AppSettings.BankSummary);
            excelApiService.AddBankSummaryToWorkSheet(result, AppSettings.BankSummary, from,  to);
        }

        public void GetBudgetSummary(DateTime period)
        {
            var result = _xeroCoreApi.Reports.BudgetSummary(period);
            excelApiService.AddExcelWorkSheet(AppSettings.BudgetSummary);
            excelApiService.AddBudgetSummaryToWorkSheet(result, AppSettings.BudgetSummary);
        }

        public void GetExecutiveSummary(DateTime period)
        {
            var result = _xeroCoreApi.Reports.ExecutiveSummary(period);
            excelApiService.AddExcelWorkSheet(AppSettings.ExecutiveSummary);
            excelApiService.AddExecutiveSummaryToWorkSheet(result, AppSettings.ExecutiveSummary);
        }
        public void GetBankStatement(string accountId, DateTime? from, DateTime? to)
        {
            var result = _xeroCoreApi.Reports.BankStatement(Guid.Parse(accountId), from, to);
            excelApiService.AddExcelWorkSheet(AppSettings.BankStatement);
            excelApiService.AddBankStatementToWorkSheet(result, AppSettings.BankStatement);
        }
        public void GetInvoices()
        {
            var result = _xeroCoreApi.Invoices.Find();
            excelApiService.AddExcelWorkSheet(AppSettings.Invoices);
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationManual;
            Globals.ThisAddIn.Application.ScreenUpdating = false;
            var contacts = _xeroCoreApi.Contacts.Find();
            excelApiService.AddInvoicesToWorkSheet(result, AppSettings.Invoices, contacts);
            Globals.ThisAddIn.Application.ScreenUpdating = true;
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationAutomatic;
        }

        public void GetPayments()
        {
            var result = _xeroCoreApi.Payments.Find();
            excelApiService.AddExcelWorkSheet(AppSettings.Payments);
            var contacts = _xeroCoreApi.Contacts.Find();
            excelApiService.AddPaymentsToWorkSheet(result, AppSettings.Payments, contacts);
        }

        public void GetContacts()
        {
            var result = _xeroCoreApi.Contacts.Find();
            excelApiService.AddExcelWorkSheet(AppSettings.Contacts);
            excelApiService.AddContactsToWorkSheet(result, AppSettings.Contacts);
        }

        public IEnumerable<Contact> GetAvialableContacts()
        {
            return _xeroCoreApi.Contacts.Find();
        }
        public IList<Account> GetBankAccounts()
        {
            var result = _xeroCoreApi.Accounts.Find();
            return result.Where(x => x.Type == AccountType.Bank).ToList();
        }
        #region helpers
        private XeroCoreApi CreateServiceWrapper()
        {
            var user = new ApiUser { Name = Environment.MachineName };
            var public_app_api = new XeroCoreApi(AppSettings.BaseXeroUrl,
                new CustomPublicAuthenticator(AppSettings.BaseXeroUrl, AppSettings.BaseXeroUrl, "oob", _tokenStore),
                new Consumer(AppSettings.ConsumerKey, AppSettings.ConsumerSecret), user,
                new DefaultMapper(), new DefaultMapper());
            return public_app_api;
        }
        #endregion
    }
}