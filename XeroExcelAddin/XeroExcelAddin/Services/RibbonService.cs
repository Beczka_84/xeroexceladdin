﻿using XeroExcelAddin.Services.Base;

namespace XeroExcelAddin.Services
{
    public interface IRibbonService
    {
        void ReserLoginButtons();
    }

    public class RibbonService : BaseService, IRibbonService
    {
        public void ReserLoginButtons()
        {
            ThisAddIn.m_Ribbon.InvalidateControl("btnLogin");
            ThisAddIn.m_Ribbon.InvalidateControl("btnLogout");
            ThisAddIn.m_Ribbon.InvalidateControl("btnOrganisation");
            ThisAddIn.m_Ribbon.InvalidateControl("btnReportAgedPayables");
            ThisAddIn.m_Ribbon.InvalidateControl("btnReportBalanceSheet");
            ThisAddIn.m_Ribbon.InvalidateControl("btnReportAgedReceivables");
            ThisAddIn.m_Ribbon.InvalidateControl("btnProfitAndLoss");
            ThisAddIn.m_Ribbon.InvalidateControl("btnTrialBalance");
            ThisAddIn.m_Ribbon.InvalidateControl("btnBankSummary");
            ThisAddIn.m_Ribbon.InvalidateControl("btnBudgetSummary");
            ThisAddIn.m_Ribbon.InvalidateControl("btnExecutiveSummary");
            ThisAddIn.m_Ribbon.InvalidateControl("btnBankStatement");
            ThisAddIn.m_Ribbon.InvalidateControl("btnInvoices");
            ThisAddIn.m_Ribbon.InvalidateControl("btnPayments");
            ThisAddIn.m_Ribbon.InvalidateControl("btnContacts");
            ThisAddIn.m_Ribbon.InvalidateControl("dropDown");
        }
    }
}
