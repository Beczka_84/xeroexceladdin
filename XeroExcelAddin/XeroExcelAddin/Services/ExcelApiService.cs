﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using XeroExcelAddin.Services.Base;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Reports;

namespace XeroExcelAddin.Services
{
    public interface IExcelApiService
    {
        void AddExcelWorkSheet(string name);
        void AddOrganisationToWorkSheet(Organisation organisation, string name);
        void AddBalanceSheetToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary);
        void AddTrialBalanceToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary);
        void AddProfitAndLossToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary);
        void AddBankSummaryToWorkSheet(Report report, string name, DateTime? from, DateTime? to);
        void AddBudgetSummaryToWorkSheet(Report report, string name);
        void AddExecutiveSummaryToWorkSheet(Report report, string name);
        void AddBankStatementToWorkSheet(Report report, string name);

        void AddAgedReceivablesToWorkSheet(Report report, string name,string contactName);
        void AddAgedPayablesToWorkSheet(Report report, string name, string contactName);
        void AddInvoicesToWorkSheet(IEnumerable<Invoice> invoices, string name, IEnumerable<Contact> contacts);
        void AddPaymentsToWorkSheet(IEnumerable<Payment> payments, string name, IEnumerable<Contact> contacts);
        void AddContactsToWorkSheet(IEnumerable<Contact> contacts, string name);
    }

    public class ExcelApiService : BaseService, IExcelApiService
    {
        private readonly ICacheService _cacheService;

        public ExcelApiService(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public void AddExcelWorkSheet(string name)
        {
            var addSheet = true;
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    addSheet = false;
                    element.Select();
                }
            }
            if (addSheet)
            {
                var newWorksheet = (Worksheet)Globals.ThisAddIn.Application.Worksheets.Add();
                newWorksheet.Name = name;
            }
        }

        public void AddOrganisationToWorkSheet(Organisation organisation, string name)
        {
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    //Headers
                    ((Range)element.Cells[1, 1]).Value2 = nameof(organisation.Name);
                    ((Range)element.Cells[1, 2]).Value2 = nameof(organisation.ApiKey);
                    ((Range)element.Cells[1, 3]).Value2 = nameof(organisation.BaseCurrency);
                    ((Range)element.Cells[1, 4]).Value2 = nameof(organisation.CountryCode);
                    ((Range)element.Cells[1, 5]).Value2 = nameof(organisation.CreatedDateUtc);
                    ((Range)element.Cells[1, 6]).Value2 = nameof(organisation.EndOfYearLockDate);
                    ((Range)element.Cells[1, 7]).Value2 = nameof(organisation.FinancialYearEndDay);
                    ((Range)element.Cells[1, 8]).Value2 = nameof(organisation.FinancialYearEndMonth);
                    ((Range)element.Cells[1, 9]).Value2 = nameof(organisation.IsDemoCompany);
                    ((Range)element.Cells[1, 10]).Value2 = nameof(organisation.LegalName);
                    ((Range)element.Cells[1, 11]).Value2 = nameof(organisation.LineOfBusiness);
                    ((Range)element.Cells[1, 12]).Value2 = nameof(organisation.PaysTax);
                    ((Range)element.Cells[1, 13]).Value2 = nameof(organisation.OrganisationStatus);
                    ((Range)element.Cells[1, 14]).Value2 = nameof(organisation.OrganisationType);
                    ((Range)element.Cells[1, 15]).Value2 = nameof(organisation.PaymentTerms);
                    ((Range)element.Cells[1, 16]).Value2 = nameof(organisation.PeriodLockDate);
                    ((Range)element.Cells[1, 17]).Value2 = nameof(organisation.RegistrationNumber);
                    ((Range)element.Cells[1, 18]).Value2 = nameof(organisation.SalesTaxBasisType);
                    ((Range)element.Cells[1, 19]).Value2 = nameof(organisation.SalesTaxPeriod);
                    ((Range)element.Cells[1, 20]).Value2 = nameof(organisation.ShortCode);
                    ((Range)element.Cells[1, 21]).Value2 = nameof(organisation.TaxNumber);
                    ((Range)element.Cells[1, 22]).Value2 = nameof(organisation.Timezone);
                    ((Range)element.Cells[1, 23]).Value2 = nameof(organisation.Version);
                    ((Range)element.Cells[1, 24]).Value2 = nameof(organisation.Addresses);
                    ((Range)element.Cells[1, 25]).Value2 = nameof(organisation.ExternalLinks);
                    ((Range)element.Cells[1, 26]).Value2 = nameof(organisation.Phones);

                    //Data
                    ((Range)element.Cells[2, 1]).Value2 = organisation.Name;
                    ((Range)element.Cells[2, 2]).Value2 = organisation.ApiKey;
                    ((Range)element.Cells[2, 3]).Value2 = organisation.BaseCurrency;
                    ((Range)element.Cells[2, 4]).Value2 = organisation.CountryCode;
                    ((Range)element.Cells[2, 5]).Value2 = organisation.CreatedDateUtc;
                    ((Range)element.Cells[2, 6]).Value2 = organisation.EndOfYearLockDate;
                    ((Range)element.Cells[2, 7]).Value2 = organisation.FinancialYearEndDay;
                    ((Range)element.Cells[2, 8]).Value2 = organisation.FinancialYearEndMonth;
                    ((Range)element.Cells[2, 9]).Value2 = organisation.IsDemoCompany;
                    ((Range)element.Cells[2, 10]).Value2 = organisation.LegalName;
                    ((Range)element.Cells[2, 11]).Value2 = organisation.LineOfBusiness;
                    ((Range)element.Cells[2, 12]).Value2 = organisation.PaysTax;
                    ((Range)element.Cells[2, 13]).Value2 = organisation.OrganisationStatus;
                    ((Range)element.Cells[2, 14]).Value2 = organisation.OrganisationType;

                    ((Range)element.Cells[2, 15]).Value2 = $"Bills: " +
                                                           $"Day {organisation.PaymentTerms?.Bills?.Day} TermType{organisation.PaymentTerms?.Bills?.TermType}" +
                                                           $" Sales: " +
                                                           $"Day {organisation.PaymentTerms?.Sales?.Day} TermType{organisation.PaymentTerms?.Sales?.TermType}";

                    ((Range)element.Cells[2, 16]).Value2 = organisation.PeriodLockDate;
                    ((Range)element.Cells[2, 17]).Value2 = organisation.RegistrationNumber;
                    ((Range)element.Cells[2, 18]).Value2 = organisation.SalesTaxBasisType;
                    ((Range)element.Cells[2, 19]).Value2 = organisation.SalesTaxPeriod;
                    ((Range)element.Cells[2, 20]).Value2 = organisation.ShortCode;
                    ((Range)element.Cells[2, 21]).Value2 = organisation.TaxNumber;
                    ((Range)element.Cells[2, 22]).Value2 = organisation.Timezone;
                    ((Range)element.Cells[2, 23]).Value2 = organisation.Version;

                    List<string> addresses = new List<string>();
                    organisation.Addresses.ForEach(x => addresses.Add($"AddressLine1:{x.AddressLine1}, AddressLine2:{x.AddressLine2}, AddressLine3:{x.AddressLine3}" +
                        $", AddressLine4:{x.AddressLine4}, AddressType:{x.AddressType.ToString()}, AttentionTo:{x.AttentionTo}" +
                        $", City:{x.City}, Country:{x.Country}, PostalCode:{x.PostalCode}, Region:{x.Region}"));

                    List<string> externalLings = new List<string>();
                    organisation.ExternalLinks.ForEach(x => externalLings.Add($"Description:{x.Description}, LinkType:{x.LinkType}, Url:{x.Url}"));

                    List<string> phones = new List<string>();
                    organisation.Phones.ForEach(x => phones.Add($"PhoneAreaCode:{x.PhoneAreaCode}, PhoneCountryCode:{x.PhoneCountryCode}, PhoneNumber:{x.PhoneNumber}, PhoneType:{x.PhoneType.ToString()}"));

                    ((Range)element.Cells[2, 24]).Value2 = string.Join(" ;", addresses);
                    ((Range)element.Cells[2, 25]).Value2 = string.Join(" ;", externalLings);
                    ((Range)element.Cells[2, 26]).Value2 = string.Join(" ;", phones);

                    element.Columns.AutoFit();
                }
            }
        }

        public void AddBalanceSheetToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 14, "tblBalanceSheet", org.Name, () => FormatAdditionalYTDBalanceSheetToWorkShee(dictionary));
        }

        public void AddTrialBalanceToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary)
        {
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationManual;
            Globals.ThisAddIn.Application.ScreenUpdating = false;
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 14, "tblTrialBalance", org.Name, () => FormatAdditionalYTDBalanceSheetToWorkShee(dictionary, 3));
            Globals.ThisAddIn.Application.ScreenUpdating = true;
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationAutomatic; ;
        }

        public void AddProfitAndLossToWorkSheet(Report report, string name, Dictionary<string, Dictionary<string, string>> dictionary)
        {
            Log.Info(JsonConvert.SerializeObject(report));
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 14, "tblProfitAndLoss", org.Name, () => FormatAdditionalYTDBalanceSheetToWorkShee(dictionary));
        }

        public void AddBankSummaryToWorkSheet(Report report, string name, DateTime? from, DateTime? to)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 6, "tblBankSummary", org.Name);
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    if (@from != null)
                    {
                        ((Range)element.Cells[1, 3]).Value ="'" + @from.Value.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (@to != null)
                    {
                        ((Range)element.Cells[1, 4]).Value = "'" + @to.Value.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    }
                }
            }
        }

        public void AddBudgetSummaryToWorkSheet(Report report, string name)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 14, "tblBudgetSummary", org.Name);

            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    for (int i = 3; i < 15; i++)
                    {
                       string oldValue = ((Range)element.Cells[1, 3]).Value.ToString();
                       var elements = oldValue.Split('-');
                        ((Range)element.Cells[1, 3]).Value = $"{elements[1]}-{elements[0]}";
                    }
                }
            }
        }

        public void AddExecutiveSummaryToWorkSheet(Report report, string name)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 5, "tblExecutiveSummary", org.Name);
        }

        public void AddAgedPayablesToWorkSheet(Report report, string name, string contactName)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 12, "tblAgedPayables", org.Name);
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    ((Range)element.Cells[1, 3]).Value2 = contactName;
                }
            }
        }

        public void AddAgedReceivablesToWorkSheet(Report report, string name, string contactName)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 12, "tblAgedReceivables", org.Name);
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    ((Range)element.Cells[1, 3]).Value2 = contactName;
                }
            }
        }

        public void AddBankStatementToWorkSheet(Report report, string name)
        {
            var org = _cacheService.Get<Organisation>(AppSettings.CurrentOrganisation);
            FormatReport(report, name, 8, "tblBankStatement", org.Name);
        }

        public void AddInvoicesToWorkSheet(IEnumerable<Invoice> invoices, string name, IEnumerable<Contact> contacts)
        {
            string tblName = "tblInvoices";
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    try
                    {
                        ListObject lo = element.ListObjects[tblName];
                        lo.Delete();
                    }
                    catch (Exception e)
                    {
                        Log.Info(e);
                    }

                    //Headers
                    ((Range)element.Cells[1, 1]).Value2 = nameof(Invoice.Id);
                    ((Range)element.Cells[1, 2]).Value2 = nameof(Invoice.Number);
                    ((Range)element.Cells[1, 3]).Value2 = $"{nameof(Contact)}.{nameof(Contact.Id)}";
                    ((Range)element.Cells[1, 4]).Value2 = $"{nameof(Contact)}.{nameof(Contact.Name)}";
                    ((Range)element.Cells[1, 5]).Value2 = nameof(Invoice.Date);
                    ((Range)element.Cells[1, 6]).Value2 = nameof(Invoice.DueDate);
                    ((Range)element.Cells[1, 7]).Value2 = nameof(Invoice.BrandingThemeId);
                    ((Range)element.Cells[1, 8]).Value2 = nameof(Invoice.Status);
                    ((Range)element.Cells[1, 9]).Value2 = nameof(Invoice.LineAmountTypes);
                    ((Range)element.Cells[1, 10]).Value2 = nameof(Invoice.TotalDiscount);

                    ((Range)element.Cells[1, 11]).Value2 = nameof(Invoice.SubTotal);
                    ((Range)element.Cells[1, 12]).Value2 = nameof(Invoice.TotalTax);
                    ((Range)element.Cells[1, 13]).Value2 = nameof(Invoice.Total);
                    ((Range)element.Cells[1, 14]).Value2 = nameof(Invoice.UpdatedDateUtc);
                    ((Range)element.Cells[1, 15]).Value2 = nameof(Invoice.CurrencyCode);
                    ((Range)element.Cells[1, 16]).Value2 = nameof(Invoice.FullyPaidOnDate);
                    ((Range)element.Cells[1, 17]).Value2 = nameof(Invoice.ExpectedPaymentDate);
                    ((Range)element.Cells[1, 18]).Value2 = nameof(Invoice.PlannedPaymentDate);
                    ((Range)element.Cells[1, 19]).Value2 = nameof(Invoice.Type);
                    ((Range)element.Cells[1, 20]).Value2 = nameof(Invoice.Reference);

                    ((Range)element.Cells[1, 21]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Id)}";
                    ((Range)element.Cells[1, 22]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Date)}";
                    ((Range)element.Cells[1, 23]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Amount)}";
                    ((Range)element.Cells[1, 24]).Value2 = $"{nameof(Payment)}.{nameof(Account)}.{nameof(Account.Id)}";
                    ((Range)element.Cells[1, 25]).Value2 = $"{nameof(Payment)}.{nameof(Account)}.{nameof(Account.Code)}";
                    ((Range)element.Cells[1, 26]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Reference)}";
                    ((Range)element.Cells[1, 27]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Invoice)}.{nameof(Invoice.Id)}";
                    ((Range)element.Cells[1, 28]).Value2 = $"{nameof(Payment)}.{nameof(Payment.CurrencyRate)}";
                    ((Range)element.Cells[1, 29]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Type)}";
                    ((Range)element.Cells[1, 30]).Value2 = $"{nameof(Payment)}.{nameof(Payment.Status)}";

                    ((Range)element.Cells[1, 31]).Value2 = $"{nameof(Payment)}.{nameof(Payment.UpdatedDateUtc)}";
                    ((Range)element.Cells[1, 32]).Value2 = $"{nameof(CreditNote)}.{nameof(CreditNote.Id)}";
                    ((Range)element.Cells[1, 33]).Value2 = $"{nameof(CreditNote)}.{nameof(CreditNote.Date)}";
                    ((Range)element.Cells[1, 34]).Value2 = $"{nameof(CreditNote)}.{nameof(CreditNote.Total)}";
                    ((Range)element.Cells[1, 35]).Value2 = $"{nameof(CreditNote)}.{nameof(CreditNote.AppliedAmount)}";
                    ((Range)element.Cells[1, 36]).Value2 = $"{nameof(CreditNote)}.{nameof(CreditNote.Number)}";
                    ((Range)element.Cells[1, 37]).Value2 = nameof(Invoice.AmountPaid);
                    ((Range)element.Cells[1, 38]).Value2 = nameof(Invoice.AmountDue);
                    ((Range)element.Cells[1, 39]).Value2 = nameof(Invoice.AmountCredited);
                    ((Range)element.Cells[1, 40]).Value2 = nameof(Invoice.Url);

                    //((Range)element.Cells[1, 41]).Value2 = nameof(Invoice.n);
                    ((Range)element.Cells[1, 42]).Value2 = nameof(Invoice.SentToContact);
                    ((Range)element.Cells[1, 43]).Value2 = nameof(Invoice.CurrencyRate);
                    ((Range)element.Cells[1, 44]).Value2 = nameof(Invoice.TotalDiscount);
                    ((Range)element.Cells[1, 45]).Value2 = nameof(Invoice.HasAttachments);
                    //((Range)element.Cells[1, 46]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 47]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 48]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 49]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 50]).Value2 = nameof(Invoice.);

                    //((Range)element.Cells[1, 51]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 52]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 53]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 54]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 55]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 56]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 57]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 58]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 59]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 60]).Value2 = nameof(Invoice.);

                    //((Range)element.Cells[1, 61]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 62]).Value2 = nameof(Invoice.);
                    //((Range)element.Cells[1, 63]).Value2 = nameof(Invoice.);

                    var rowNo = 2;
                    foreach (var invoice in invoices)
                    {
                        ((Range)element.Cells[rowNo, 1]).Value2 = invoice.Id.ToString();
                        ((Range)element.Cells[rowNo, 2]).Value2 = invoice.Number;
                        ((Range)element.Cells[rowNo, 3]).Value2 = invoice?.Contact?.Id.ToString();
                        ((Range)element.Cells[rowNo, 4]).Value2 = invoice?.Contact?.Name;
                        ((Range)element.Cells[rowNo, 5]).Value2 = invoice.Date;
                        ((Range)element.Cells[rowNo, 6]).Value2 = invoice.DueDate;
                        ((Range)element.Cells[rowNo, 7]).Value2 = invoice.BrandingThemeId.ToString();
                        ((Range)element.Cells[rowNo, 8]).Value2 = invoice.Status.ToString();
                        ((Range)element.Cells[rowNo, 9]).Value2 = invoice.LineAmountTypes.ToString();
                        ((Range)element.Cells[rowNo, 10]).Value2 = invoice.TotalDiscount;

                        ((Range)element.Cells[rowNo, 11]).Value2 = invoice.SubTotal;
                        ((Range)element.Cells[rowNo, 12]).Value2 = invoice.TotalTax;
                        ((Range)element.Cells[rowNo, 13]).Value2 = invoice.Total;
                        ((Range)element.Cells[rowNo, 14]).Value2 = invoice.UpdatedDateUtc;
                        ((Range)element.Cells[rowNo, 15]).Value2 = invoice.CurrencyCode;
                        ((Range)element.Cells[rowNo, 16]).Value2 = invoice.FullyPaidOnDate;
                        ((Range)element.Cells[rowNo, 17]).Value2 = invoice.ExpectedPaymentDate;
                        ((Range)element.Cells[rowNo, 18]).Value2 = invoice.PlannedPaymentDate;
                        ((Range)element.Cells[rowNo, 19]).Value2 = invoice.Type.ToString();
                        ((Range)element.Cells[rowNo, 20]).Value2 = invoice.Reference;

                        ((Range)element.Cells[rowNo, 21]).Value2 = invoice?.Payments?.FirstOrDefault()?.Id.ToString();
                        ((Range)element.Cells[rowNo, 22]).Value2 = invoice?.Payments?.FirstOrDefault()?.Date;
                        ((Range)element.Cells[rowNo, 23]).Value2 = invoice?.Payments?.FirstOrDefault()?.Amount;
                        ((Range)element.Cells[rowNo, 24]).Value2 = invoice?.Payments?.FirstOrDefault()?.Account?.Id.ToString() ?? "";
                        ((Range)element.Cells[rowNo, 25]).Value2 = invoice?.Payments?.FirstOrDefault()?.Account?.Code;
                        ((Range)element.Cells[rowNo, 26]).Value2 = invoice?.Payments?.FirstOrDefault()?.Reference;
                        ((Range)element.Cells[rowNo, 27]).Value2 = invoice?.Payments?.FirstOrDefault()?.Invoice?.Id.ToString() ?? "";
                        ((Range)element.Cells[rowNo, 28]).Value2 = invoice?.Payments?.FirstOrDefault()?.CurrencyRate;
                        ((Range)element.Cells[rowNo, 29]).Value2 = invoice?.Payments?.FirstOrDefault()?.Type.ToString();
                        ((Range)element.Cells[rowNo, 30]).Value2 = invoice?.Payments?.FirstOrDefault()?.Status.ToString();

                        ((Range)element.Cells[rowNo, 31]).Value2 = invoice?.Payments?.FirstOrDefault()?.UpdatedDateUtc;
                        ((Range)element.Cells[rowNo, 32]).Value2 = invoice?.CreditNotes?.FirstOrDefault()?.Id.ToString();
                        ((Range)element.Cells[rowNo, 33]).Value2 = invoice?.CreditNotes?.FirstOrDefault()?.Date;
                        ((Range)element.Cells[rowNo, 34]).Value2 = invoice?.CreditNotes?.FirstOrDefault()?.Total;
                        ((Range)element.Cells[rowNo, 35]).Value2 = invoice?.CreditNotes?.FirstOrDefault()?.AppliedAmount;
                        ((Range)element.Cells[rowNo, 36]).Value2 = invoice?.CreditNotes?.FirstOrDefault()?.Number;
                        ((Range)element.Cells[rowNo, 37]).Value2 = invoice.AmountPaid;
                        ((Range)element.Cells[rowNo, 38]).Value2 = invoice.AmountDue;
                        ((Range)element.Cells[rowNo, 39]).Value2 = invoice.AmountCredited;
                        ((Range)element.Cells[rowNo, 40]).Value2 = invoice.Url;

                        //((Range)element.Cells[rowNo, 41]).Value2 = invoice.;
                        ((Range)element.Cells[rowNo, 42]).Value2 = invoice.SentToContact;
                        ((Range)element.Cells[rowNo, 43]).Value2 = invoice.CurrencyRate;
                        ((Range)element.Cells[rowNo, 44]).Value2 = invoice.TotalDiscount;
                        ((Range)element.Cells[rowNo, 45]).Value2 = invoice.HasAttachments;
                        //((Range)element.Cells[rowNo, 46]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 47]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 48]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 49]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 50]).Value2 = invoice.;

                        //((Range)element.Cells[rowNo, 51]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 52]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 53]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 54]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 55]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 56]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 57]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 58]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 59]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 60]).Value2 = invoice.;

                        //((Range)element.Cells[rowNo, 61]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 62]).Value2 = invoice.;
                        //((Range)element.Cells[rowNo, 63]).Value2 = invoice.;
                        rowNo++;
                    }
                    element.ListObjects.Add(XlListObjectSourceType.xlSrcRange, element
                     .Range[element.Cells[1, 1], element.Cells[rowNo, 45]], null, XlYesNoGuess.xlYes)
                     .Name = tblName;
                    ((Range)element.Columns["E:E"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["F:F"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["P:P"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["Q:Q"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["R:R"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["V:V"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["AE:AG"]).NumberFormat = "m/d/yyyy";
                    ((Range)element.Columns["AE:AE"]).NumberFormat = "m/d/yyyy";
                    element.Columns.AutoFit();
                }
            }
        }

        public void AddPaymentsToWorkSheet(IEnumerable<Payment> payments, string name, IEnumerable<Contact> contacts)
        {
            string tblName = "tblPayments";

            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    try
                    {
                        ListObject lo = element.ListObjects[tblName];
                        lo.Delete();
                    }
                    catch (Exception e)
                    {
                        Log.Info(e);
                    }
                    //Headers
                    ((Range)element.Cells[1, 1]).Value2 = $"{nameof(Contact)}.{nameof(Contact.Name)}";
                    ((Range)element.Cells[1, 2]).Value2 = nameof(Payment.Date);
                    ((Range)element.Cells[1, 3]).Value2 = $"{nameof(Payment.Amount)}";
                    ((Range)element.Cells[1, 4]).Value2 = $"{nameof(Payment)}.{nameof(Account.Id)}";
                    ((Range)element.Cells[1, 5]).Value2 = $"{nameof(Payment)}.{nameof(Account.Code)}";
                    ((Range)element.Cells[1, 6]).Value2 = nameof(Payment.Reference);
                    ((Range)element.Cells[1, 7]).Value2 = $"{nameof(Payment)}.{nameof(Invoice.Id)}";
                    ((Range)element.Cells[1, 8]).Value2 = nameof(Payment.CurrencyRate);
                    ((Range)element.Cells[1, 9]).Value2 = nameof(Payment.Type);
                    ((Range)element.Cells[1, 10]).Value2 = nameof(Payment.Status);
                    ((Range)element.Cells[1, 11]).Value2 = nameof(Payment.UpdatedDateUtc);

                    var rowNo = 2;
                    foreach (var payment in payments)
                    {
                        ((Range)element.Cells[rowNo, 1]).Value2 = contacts.FirstOrDefault(x=>x.Id == payment.Invoice.Contact.Id)?.Name ?? string.Empty;
                        ((Range)element.Cells[rowNo, 2]).Value2 = payment.Date;
                        ((Range)element.Cells[rowNo, 3]).Value2 = payment?.Amount;
                        ((Range)element.Cells[rowNo, 4]).Value2 = payment?.Account?.Id.ToString();
                        ((Range)element.Cells[rowNo, 5]).Value2 = payment?.Account?.Code;
                        ((Range)element.Cells[rowNo, 6]).Value2 = payment.Reference;
                        ((Range)element.Cells[rowNo, 7]).Value2 = payment?.Invoice.Id.ToString();
                        ((Range)element.Cells[rowNo, 8]).Value2 = payment.CurrencyRate;
                        ((Range)element.Cells[rowNo, 9]).Value2 = payment.Type.ToString();
                        ((Range)element.Cells[rowNo, 10]).Value2 = payment.Status;
                        ((Range)element.Cells[rowNo, 11]).Value2 = payment.UpdatedDateUtc;
                        rowNo++;
                    }
                    element.ListObjects.Add(XlListObjectSourceType.xlSrcRange, element
                     .Range[element.Cells[1, 1], element.Cells[rowNo, 11]], null, XlYesNoGuess.xlYes)
                     .Name = tblName;
                    element.Columns.AutoFit();
                }
            }
        }

        public void AddContactsToWorkSheet(IEnumerable<Contact> contacts, string name)
        {
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    //Headers
                    ((Range)element.Cells[1, 1]).Value2 = nameof(Contact.Id);
                    ((Range)element.Cells[1, 2]).Value2 = nameof(Contact.Name);

                    var rowNo = 2;
                    foreach (var contact in contacts)
                    {
                        ((Range)element.Cells[rowNo, 1]).Value2 = contact.Id.ToString();
                        ((Range)element.Cells[rowNo, 2]).Value2 = contact.Name;
                        rowNo++;
                    }
                    element.ListObjects.Add(XlListObjectSourceType.xlSrcRange, element
                     .Range[element.Cells[1, 1], element.Cells[(rowNo), 2]], null, XlYesNoGuess.xlYes)
                     .Name = "tblContacts";
                    element.Columns.AutoFit();
                }
            }
        }

        #region helpers

        private void FormatAdditionalYTDBalanceSheetToWorkShee(Dictionary<string, Dictionary<string, string>> dictionary, int colNo = 4)
        {
            var element = (Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            var rowNo = 2;
            int lastRow = element.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing).Row;
            foreach (var keyValuePair in dictionary)
            {
                ((Range)element.Cells[rowNo, colNo]).Value2 = keyValuePair.Key.ToString();
                for (int i = 1; i < lastRow + 1; i++)
                {
                    string value = (string)((Range)element.Cells[i, 2]).Value2;
                    var item = keyValuePair.Value.FirstOrDefault(x => x.Key == value?.Trim());
                    if (!item.Equals(default(KeyValuePair<string, string>)))
                    {
                        ((Range)element.Cells[i, colNo]).Value2 = item.Value;
                    }
                }
                colNo++;
            }
        }

        private void FormatReport(Report report, string name, int columns, string tableName, string orgName, System.Action aditionalStep = null)
        {
            foreach (var sheet in Globals.ThisAddIn.Application.Worksheets)
            {
                var element = (Worksheet)sheet;
                if (element.Name == name)
                {
                    try
                    {
                        ListObject lo = element.ListObjects[tableName];
                        lo.Delete();
                    }
                    catch (Exception e)
                    {
                        Log.Info(e);
                    }

                    //Headers
                    //((Range)element.Cells[1, 1]).Value2 = nameof(report.ReportID);
                    //((Range)element.Cells[1, 2]).Value2 = nameof(report.ReportName);
                    //((Range)element.Cells[1, 3]).Value2 = nameof(report.ReportType);
                    //((Range)element.Cells[1, 4]).Value2 = nameof(report.ReportTitles);
                    //((Range)element.Cells[1, 5]).Value2 = nameof(report.ReportDate);
                    //((Range)element.Cells[1, 6]).Value2 = nameof(report.UpdatedDateUTC);
                    //((Range)element.Cells[1, 7]).Value2 = nameof(report.Attributes);
                    //((Range)element.Cells[1, 8]).Value2 = nameof(report.Fields);

                    //((Range)element.Cells[2, 1]).Value2 = report.ReportID;
                    ((Range)element.Cells[1, 1]).Value2 = orgName;
                    ((Range)element.Cells[1, 2]).Value2 = report.ReportName;
                    //((Range)element.Cells[2, 3]).Value2 = report.ReportType.ToString();
                    //((Range)element.Cells[2, 4]).Value2 = string.Join(" ", report.ReportTitles);
                  

                    var rowNo = 2;
                    bool shoudAddRow = false;
                    foreach (var row in report.Rows)
                    {
                        if (row.RowType == "Header")
                        {
                            //((Range)element.Cells[rowNo, 1]).Value2 = row.RowType;
                            //((Range)element.Cells[rowNo, 1]).Value2 = row.Title;
                            FillCels(element, rowNo, row, 2);
                        }
                        shoudAddRow = true;
                        if (row.RowType == "Section")
                        {
                            //((Range)element.Cells[rowNo, 2]).Value2 = row.RowType;
                            //((Range)element.Cells[rowNo, 1]).Value2 = row.Title;

                            if (row.Rows != null)
                            {
                                rowNo++;
                                foreach (var datRow in row.Rows)
                                {
                                    if (datRow.RowType == "Row")
                                    {
                                        //((Range)element.Cells[rowNo, 3]).Value2 = datRow.RowType;
                                        ((Range)element.Cells[rowNo, 1]).Value2 = row.Title;
                                        ((Range)element.Cells[rowNo, 2]).Value2 = datRow.Title;
                                        FillCels(element, rowNo, datRow, 2);
                                        rowNo++;
                                        shoudAddRow = false;
                                    }

                                    if (datRow.RowType == "SummaryRow")
                                    {
                                        //((Range)element.Cells[rowNo, 3]).Value2 = datRow.RowType;
                                        ((Range)element.Cells[rowNo, 1]).Value2 = row.Title;
                                        ((Range)element.Cells[rowNo, 2]).Value2 = datRow.Title;
                                        FillCels(element, rowNo, datRow, 2);
                                        rowNo++;
                                        shoudAddRow = false;
                                    }
                                }
                            }
                        }
                        rowNo++;
                    }

                    aditionalStep?.Invoke();

                    if (rowNo == 3)
                    {
                        rowNo = 5;
                    }
                    element.ListObjects.Add(XlListObjectSourceType.xlSrcRange, element
                        .Range[element.Cells[2, 1], element.Cells[(rowNo - 2), columns]], null, XlYesNoGuess.xlYes)
                        .Name = tableName;
                    element.Columns.AutoFit();
                }
            }
        }

        private void FillCels(Worksheet element, int rowNo, ReportRow row, int colNo)
        {
            if (row.Cells != null)
            {
                foreach (var cell in row.Cells)
                {
                    ((Range)element.Cells[rowNo, colNo]).Value = cell.Value;
                    //if (cell.Attributes != null)
                    //{
                    //    StringBuilder builder = new StringBuilder();
                    //    foreach (var att in cell.Attributes)
                    //    {
                    //        builder.Append($"Attribute-Id {att.Id} Attribute-Value {att.Value}");
                    //        builder.AppendLine();
                    //    }
                    //    ((Range)element.Cells[rowNo, colNo]).AddComment(builder.ToString());
                    //}
                    colNo++;
                }
            }
        }
        #endregion
    }
}