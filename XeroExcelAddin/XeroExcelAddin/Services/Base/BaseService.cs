﻿namespace XeroExcelAddin.Services.Base
{
    public interface IBaseService
    {
        
    }

    public class BaseService : IBaseService
    {
        public log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
