﻿using AutoMapper;

namespace XeroExcelAddin
{
    public static class MapperModule
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                //cfg.CreateMap<config, ConfigViewModel>();
                //cfg.CreateMap<AdditionalFileInfoInfo, FiledEmail>();
                //cfg.CreateMap<whitelist, WhiteListViewModel>();
                //cfg.CreateMap<WhiteListViewModel, whitelist>();
                //cfg.CreateMap<xero_contacts, XeroContactsViewModel>()
                //    .ForMember(x => x.form_display, opt => opt.ResolveUsing<CustomFormDisplayNameResolver>())
                //    .ForMember(x => x.group_name, opt => opt.ResolveUsing<GroupNameResolver>())
                //    .ForMember(x => x.folder_path, opt => opt.ResolveUsing<CustomFolderPathNameResolver>());
                //cfg.CreateMap<xero_groups, XeroGroupsViewModel>();
            });
        }
    }
}
