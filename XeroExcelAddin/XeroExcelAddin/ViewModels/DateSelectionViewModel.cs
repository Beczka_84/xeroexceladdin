﻿using System;
using System.Windows.Input;
using Prism.Commands;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.ViewModels
{
    public class DateSelectionViewModel : BaseViewModel
    {
        private DateSelection _inst = null;
        public string MainTitle => "Dashboard Diva - Date selection";
        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }
            set
            {
                SetProperty(ref _date, value);
                OnPropertyChanged(() => Date);
            }
        }
        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public DateSelectionViewModel()
        {
            OkCommand = new DelegateCommand(Ok);
            CancelCommand = new DelegateCommand(Cancel);
        }

        private void Ok()
        {
            _inst = DateSelection.GetInstance();
            _inst.Close();
            _inst = null;
        }

        private void Cancel()
        {
            Date = null;
            _inst = DateSelection.GetInstance();
            _inst.Close();
            _inst = null;
        }
    }
}
