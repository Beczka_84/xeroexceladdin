﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.ViewModels
{
    public class BankViewModel : BaseViewModel
    {
        private BankSummary _inst = null;
        public string MainTitle => "Bank summary";
        private DateTime? _from = null;
        private DateTime? _to = null;

        public DateTime? From
        {
            get { return _from; }
            set
            {
                SetProperty(ref _from, value);
                OnPropertyChanged(() => From);
            }
        }

        public DateTime? To
        {
            get { return _to; }
            set
            {
                SetProperty(ref _to, value);
                OnPropertyChanged(() => To);
            }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public BankViewModel()
        {
            OkCommand = new DelegateCommand(Ok);
            CancelCommand = new DelegateCommand(Cancel);
        }
        private void Ok()
        {
            _inst = BankSummary.GetInstance();
            _inst.Close();
        }

        private void Cancel()
        {
            _inst = BankSummary.GetInstance();
            _inst.Close();
        }

    }
}
