﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Autofac;
using Prism.Commands;
using XeroExcelAddin.Services;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.ViewModels
{
    public class ContactViewModel : BaseViewModel
    {
        private Contact _inst = null;
        public string MainTitle => "Dashboard Diva - Contact selection";
        private List<Xero.Api.Core.Model.Contact> _availableContacts;
        public List<Xero.Api.Core.Model.Contact> AvailableContacts
        {
            get { return _availableContacts; }
            set
            {
                SetProperty(ref _availableContacts, value);
                OnPropertyChanged(() => AvailableContacts);
            }
        }
        private Xero.Api.Core.Model.Contact _selectedContactId;
        public Xero.Api.Core.Model.Contact SelectedContactId
        {
            get { return _selectedContactId; }
            set
            {
                SetProperty(ref _selectedContactId, value);
                OnPropertyChanged(() => SelectedContactId);
            }
        }
        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }
            set
            {
                SetProperty(ref _date, value);
                OnPropertyChanged(() => Date);
            }
        }
        public string ContactId => _selectedContactId?.Id.ToString() ?? null;
        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ContactViewModel()
        {
            OkCommand = new DelegateCommand(Ok);
            CancelCommand = new DelegateCommand(Cancel);
            AvailableContacts = new List<Xero.Api.Core.Model.Contact>();
            var contacts = Globals.ThisAddIn.Core.Resolve<IXeroService>().GetAvialableContacts().ToList();
            AvailableContacts.AddRange(contacts.OrderBy(x=>x.Name));
        }

        private void Ok()
        {
            _inst = Contact.GetInstance();
            _inst.Close();
        }

        private void Cancel()
        {
            _inst = Contact.GetInstance();
            _inst.Close();
        }
    }
}
