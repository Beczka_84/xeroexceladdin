﻿using System.Windows.Input;
using Prism.Commands;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.ViewModels
{
    public class CodeAuthenticationViewModel : BaseViewModel
    {
        private CodeAuthentication _inst = null;
        private string _code = null;
        public string MainTitle => "Dashboard Diva - Code Authentication";
        public string Code
        {
            get { return _code; }
            set
            {
                SetProperty(ref _code, value);
                OnPropertyChanged(() => Code);
            }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public CodeAuthenticationViewModel()
        {
            OkCommand = new DelegateCommand(Ok);
            CancelCommand = new DelegateCommand(Cancel);
        }


        private void Ok()
        {
            _inst = CodeAuthentication.GetInstance();
            _inst.Close();
        }

        private void Cancel()
        {
            _inst = CodeAuthentication.GetInstance();
            _inst.Close();
        }
    }
}
