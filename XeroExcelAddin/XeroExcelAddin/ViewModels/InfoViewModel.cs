﻿using System.Windows.Input;
using Autofac;
using Prism.Commands;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;
using IXeroService = XeroExcelAddin.Services.IXeroService;

namespace XeroExcelAddin.ViewModels
{
    public class InfoViewModel : BaseViewModel
    {
        private string _message;
        private string _title;
        private bool _closeBtnVisiblility;
        private Info _inst = null;

        public bool CloseBtnVisiblility
        {
            get { return _closeBtnVisiblility; }
            set
            {
                SetProperty(ref _closeBtnVisiblility, value);
                OnPropertyChanged(() => CloseBtnVisiblility);
            }
        }

        public string Message
        {
            get { return _message; }
            set
            {
                SetProperty(ref _message, value);
                OnPropertyChanged(() => Message);
            }
        }
        public string Title
        {
            get { return _title; }
            set
            {
                SetProperty(ref _title, value);
                OnPropertyChanged(() => Title);
            }
        }
        public ICommand CloseCommand { get; private set; }

        public InfoViewModel(string message, string title)
        {
            Message = message;
            Title = title;
            CloseCommand = new DelegateCommand(Close);
            if(message.Contains("Please wait"))
            {
                CloseBtnVisiblility = false;
            }
            else
            {
                CloseBtnVisiblility = true;
            }
        }

        private void Close()
        {
            _inst = Info.GetInstance();
            _inst.Close();
        }
    }
}
