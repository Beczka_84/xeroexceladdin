﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Autofac;
using Prism.Commands;
using XeroExcelAddin.Services;
using XeroExcelAddin.ViewModels.Base;
using XeroExcelAddin.Views;

namespace XeroExcelAddin.ViewModels
{
    public class BankStatementViewModel : BaseViewModel
    {
        private BankStatement _inst;
        public string MainTitle => "Dashboard Diva - Bank Statement";
        private DateTime? _from = null;
        private DateTime? _to = null;

        public DateTime? From
        {
            get { return _from; }
            set
            {
                SetProperty(ref _from, value);
                OnPropertyChanged(() => From);
            }
        }
        public DateTime? To
        {
            get { return _to; }
            set
            {
                SetProperty(ref _to, value);
                OnPropertyChanged(() => To);
            }
        }

        private List<Xero.Api.Core.Model.Account> _availableAccounts;
        public List<Xero.Api.Core.Model.Account> AvailableAccounts
        {
            get { return _availableAccounts; }
            set
            {
                SetProperty(ref _availableAccounts, value);
                OnPropertyChanged(() => AvailableAccounts);
            }
        }
        private Xero.Api.Core.Model.Account _selectedAccount;
        public Xero.Api.Core.Model.Account SelectedAccount
        {
            get { return _selectedAccount; }
            set
            {
                SetProperty(ref _selectedAccount, value);
                OnPropertyChanged(() => SelectedAccount);
            }
        }

        public string AccountId => _selectedAccount?.Id.ToString() ?? null;

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public BankStatementViewModel()
        {
            OkCommand = new DelegateCommand(Ok);
            CancelCommand = new DelegateCommand(Cancel);
            AvailableAccounts = new List<Xero.Api.Core.Model.Account>();
            var contacts = Globals.ThisAddIn.Core.Resolve<IXeroService>().GetBankAccounts().ToList();
            AvailableAccounts.AddRange(contacts);
        }
        private void Ok()
        {
            _inst = BankStatement.GetInstance();
            _inst.Close();
        }

        private void Cancel()
        {
            _inst = BankStatement.GetInstance();
            _inst.Close();
        }

    }
}
