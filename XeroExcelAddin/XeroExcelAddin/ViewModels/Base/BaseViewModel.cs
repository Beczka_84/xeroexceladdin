﻿using Prism.Mvvm;

namespace XeroExcelAddin.ViewModels.Base
{
    public class BaseViewModel : BindableBase
    {
        public log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}
