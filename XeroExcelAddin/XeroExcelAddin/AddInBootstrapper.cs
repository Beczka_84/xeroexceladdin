﻿using System.Reflection;
using Autofac;
using Autofac.Extras.DynamicProxy;
using XeroExcelAddin.Infrastructure;

namespace XeroExcelAddin
{
    public static class AddInBootstrapper
    {
        public static IContainer InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.Load("XeroExcelAddin"))
               .Where(t => t.Name.Contains("Service") || t.Name.Contains("TokenStore"))
               .AsImplementedInterfaces()
               .EnableInterfaceInterceptors()
               .InterceptedBy(typeof(LoggingAspect));
            builder.RegisterType<LoggingAspect>();
            return builder.Build();
        }
    }
}
