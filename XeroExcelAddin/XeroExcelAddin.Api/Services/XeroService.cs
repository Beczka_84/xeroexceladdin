﻿using System;
using System.Linq;
using Xero.Api.Core;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using XeroExcelAddin.Api.Infrastructure.Xero;
using XeroExcelAddin.Api.Services.Base;

namespace XeroExcelAddin.Api.Services
{
    public interface IXeroService
    {
        void Test(object dialog);
    }

    public class XeroService : BaseService, IXeroService
    {
        private readonly ITokenStore _tokenStore;

        public XeroService(ITokenStore tokenStore)
        {
            _tokenStore = tokenStore;
        }

        public void Test(object dialog)
        {
            var user = new ApiUser {Name = Environment.MachineName};

            var public_app_api = new XeroCoreApi(AppSettings.BaseXeroUrl,
                new CustomPublicAuthenticator(dialog, AppSettings.BaseXeroUrl, AppSettings.BaseXeroUrl, "oob",
                    _tokenStore),
                new Consumer(AppSettings.ConsumerKey, AppSettings.ConsumerSecret), user,
                new DefaultMapper(), new DefaultMapper());

            var public_contacts = public_app_api.Accounts.Find().ToList();
        }
    }
}