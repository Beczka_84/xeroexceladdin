﻿using System;
using System.Runtime.Caching;
using XeroExcelAddin.Api.Services.Base;

namespace XeroExcelAddin.Api.Services
{
    public interface ICacheService
    {
        T Get<T>(string name) where T : class;
        void Set<T>(string name, T data) where T : class;

    }

    public class CacheService : BaseService, ICacheService
    {
        public T Get<T>(string name) where T : class
        {
            MemoryCache memoryCache = MemoryCache.Default;
            T result = memoryCache.Get(name) as T;
            return result;
        }

        public void Set<T>(string name, T data) where T : class
        {
            if (data == null)
            {
                MemoryCache memoryCache = MemoryCache.Default;
                if (memoryCache.Contains(name))
                {
                    memoryCache.Remove(name);
                }
            }
            else
            {
                MemoryCache memoryCache = MemoryCache.Default;
                memoryCache.Add(name, data, DateTimeOffset.UtcNow.AddMinutes(30));
            }
        }
    }
}
