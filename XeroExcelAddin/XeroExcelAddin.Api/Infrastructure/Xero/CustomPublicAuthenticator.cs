﻿using System;
using System.Reflection;
using Xero.Api.Example.Applications.Public;
using Xero.Api.Infrastructure.Interfaces;

namespace XeroExcelAddin.Api.Infrastructure.Xero
{
    public class CustomPublicAuthenticator : PublicAuthenticator
    {
        private readonly object _dialog;
        public CustomPublicAuthenticator(object dialog, string baseUri, string tokenUri, string callBackUrl, ITokenStore store,
            string scope = null) : base(baseUri, tokenUri, callBackUrl, store, scope)
        {
            _dialog = dialog;
        }

        protected override string AuthorizeUser(IToken token)
        {
            var code = base.AuthorizeUser(token);
            if (string.IsNullOrEmpty(code))
            {
                Type type = _dialog.GetType();
                MethodInfo methodInfo = type.GetMethod("ShowDialog");
                var dialog = methodInfo.Invoke(_dialog, null);

                if (dialog != null)
                {
                    var context = type.GetProperty("DataContext").GetValue(_dialog, null);
                }
            }
            //var aa = _showCodeAuthenticatio();
            var x = "2096406";
            return x;
        }
    }
}