﻿using System;
using Xero.Api.Infrastructure.Interfaces;
using XeroExcelAddin.Api.Services;

namespace XeroExcelAddin.Api.Infrastructure.Xero
{
    public class CustomTokenStore : ITokenStore
    {
        private readonly ICacheService _cacheService;

        public CustomTokenStore(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public IToken Find(string user)
        {
            return _cacheService.Get<IToken>(user);
        }

        public void Add(IToken token)
        {
            _cacheService.Set<IToken>(Environment.MachineName, token);
        }

        public void Delete(IToken token)
        {
            _cacheService.Set<IToken>(Environment.MachineName, null);
        }
    }
}