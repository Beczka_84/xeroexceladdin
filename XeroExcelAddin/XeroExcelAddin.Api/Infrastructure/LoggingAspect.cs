﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy;

namespace XeroExcelAddin.Api.Infrastructure
{
    public class LoggingAspect : IInterceptor
    {
    //    log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void Intercept(IInvocation invocation)
        {
    //        var listOfArgumumentValues = new List<string>();
    //        var argsObj = invocation.Arguments.Where(y => y is ILoginMarker).Select(x => x).ToList();
    //        argsObj.ForEach(obj => listOfArgumumentValues.AddRange(obj.GetPropValues()));
    //        listOfArgumumentValues.AddRange(invocation.Arguments.Where(y => y is string).Select(a => (a ?? string.Empty).ToString()).ToArray());
    //        listOfArgumumentValues.AddRange(invocation.Arguments.Where(y => y.GetType().IsValueType).Select(a => (a ?? string.Empty).ToString()).ToArray());
    //        log.Debug($"{invocation.Method.Name} method invoked with parameters : {string.Join(",", listOfArgumumentValues)}");
    //        try
    //        {
                invocation.Proceed();
    //        }
    //        catch (Exception e)
    //        {
    //            log.Error(e);
    //        }
        }
    }
}
