﻿using System.Reflection;
using Autofac;
using Autofac.Extras.DynamicProxy;
using XeroExcelAddin.Api.Infrastructure;

namespace XeroExcelAddin.Api
{
    public class ApiModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("XeroExcelAddin.Api"))
                .Where(t => t.Name.Contains("Service") || t.Name.Contains("TokenStore"))
                .AsImplementedInterfaces()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(LoggingAspect));
            builder.RegisterType<LoggingAspect>();
            base.Load(builder);
        }
    }
}
