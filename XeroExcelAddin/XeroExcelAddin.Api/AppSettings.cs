﻿namespace XeroExcelAddin.Api
{
    public static class AppSettings
    {
        public static string BaseXeroUrl => System.Configuration.ConfigurationManager.AppSettings.Get("baseXeroUrl");
        public static string ConsumerKey => System.Configuration.ConfigurationManager.AppSettings.Get("consumerKey");
        public static string ConsumerSecret => System.Configuration.ConfigurationManager.AppSettings.Get("consumerSecret");
    }
}
