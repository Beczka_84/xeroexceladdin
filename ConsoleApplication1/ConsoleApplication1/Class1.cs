﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApplication1
{
    [XmlRoot(ElementName = "meta", Namespace = "http://ops.epo.org")]
    public class PatentBilboDataMeta
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "document-id", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataDocumentid
    {
        [XmlElement(ElementName = "country", Namespace = "http://www.epo.org/exchange")]
        public string Country { get; set; }
        [XmlElement(ElementName = "doc-number", Namespace = "http://www.epo.org/exchange")]
        public string Docnumber { get; set; }
        [XmlElement(ElementName = "kind", Namespace = "http://www.epo.org/exchange")]
        public string Kind { get; set; }
        [XmlElement(ElementName = "date", Namespace = "http://www.epo.org/exchange")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "document-id-type")]
        public string Documentidtype { get; set; }
    }

    [XmlRoot(ElementName = "publication-reference", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataPublicationreference
    {
        [XmlElement(ElementName = "document-id", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataDocumentid> Documentid { get; set; }
    }

    [XmlRoot(ElementName = "classification-ipc", Namespace = "http://www.epo.org/exchange")]
    public class Classificationipc
    {
        [XmlElement(ElementName = "text", Namespace = "http://www.epo.org/exchange")]
        public List<string> Text { get; set; }
    }

    [XmlRoot(ElementName = "classification-ipcr", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataClassificationipcr
    {
        [XmlElement(ElementName = "text", Namespace = "http://www.epo.org/exchange")]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
    }

    [XmlRoot(ElementName = "classifications-ipcr", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataClassificationsipcr
    {
        [XmlElement(ElementName = "classification-ipcr", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataClassificationipcr> Classificationipcr { get; set; }
    }

    [XmlRoot(ElementName = "classification-scheme", Namespace = "http://www.epo.org/exchange")]
    public class Classificationscheme
    {
        [XmlAttribute(AttributeName = "office")]
        public string Office { get; set; }
        [XmlAttribute(AttributeName = "scheme")]
        public string Scheme { get; set; }
    }

    [XmlRoot(ElementName = "patent-classification", Namespace = "http://www.epo.org/exchange")]
    public class Patentclassification
    {
        [XmlElement(ElementName = "classification-scheme", Namespace = "http://www.epo.org/exchange")]
        public Classificationscheme Classificationscheme { get; set; }
        [XmlElement(ElementName = "section", Namespace = "http://www.epo.org/exchange")]
        public string Section { get; set; }
        [XmlElement(ElementName = "class", Namespace = "http://www.epo.org/exchange")]
        public string Class { get; set; }
        [XmlElement(ElementName = "subclass", Namespace = "http://www.epo.org/exchange")]
        public string Subclass { get; set; }
        [XmlElement(ElementName = "main-group", Namespace = "http://www.epo.org/exchange")]
        public string Maingroup { get; set; }
        [XmlElement(ElementName = "subgroup", Namespace = "http://www.epo.org/exchange")]
        public string Subgroup { get; set; }
        [XmlElement(ElementName = "classification-value", Namespace = "http://www.epo.org/exchange")]
        public string Classificationvalue { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
    }

    [XmlRoot(ElementName = "patent-classifications", Namespace = "http://www.epo.org/exchange")]
    public class Patentclassifications
    {
        [XmlElement(ElementName = "patent-classification", Namespace = "http://www.epo.org/exchange")]
        public List<Patentclassification> Patentclassification { get; set; }
    }

    [XmlRoot(ElementName = "application-reference", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataApplicationreference
    {
        [XmlElement(ElementName = "document-id", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataDocumentid> Documentid { get; set; }
        [XmlAttribute(AttributeName = "doc-id")]
        public string Docid { get; set; }
    }

    [XmlRoot(ElementName = "priority-claim", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataPriorityclaim
    {
        [XmlElement(ElementName = "document-id", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataDocumentid> Documentid { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
        [XmlAttribute(AttributeName = "kind")]
        public string Kind { get; set; }
    }

    [XmlRoot(ElementName = "priority-claims", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataPriorityclaims
    {
        [XmlElement(ElementName = "priority-claim", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataPriorityclaim Priorityclaim { get; set; }
    }

    [XmlRoot(ElementName = "applicant-name", Namespace = "http://www.epo.org/exchange")]
    public class Applicantname
    {
        [XmlElement(ElementName = "name", Namespace = "http://www.epo.org/exchange")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "applicant", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataApplicant
    {
        [XmlElement(ElementName = "applicant-name", Namespace = "http://www.epo.org/exchange")]
        public Applicantname Applicantname { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
        [XmlAttribute(AttributeName = "data-format")]
        public string Dataformat { get; set; }
    }

    [XmlRoot(ElementName = "applicants", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataApplicants
    {
        [XmlElement(ElementName = "applicant", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataApplicant> Applicant { get; set; }
    }

    [XmlRoot(ElementName = "inventor-name", Namespace = "http://www.epo.org/exchange")]
    public class Inventorname
    {
        [XmlElement(ElementName = "name", Namespace = "http://www.epo.org/exchange")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "inventor", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataInventor
    {
        [XmlElement(ElementName = "inventor-name", Namespace = "http://www.epo.org/exchange")]
        public Inventorname Inventorname { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
        [XmlAttribute(AttributeName = "data-format")]
        public string Dataformat { get; set; }
    }

    [XmlRoot(ElementName = "inventors", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataInventors
    {
        [XmlElement(ElementName = "inventor", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataInventor> Inventor { get; set; }
    }

    [XmlRoot(ElementName = "parties", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataParties
    {
        [XmlElement(ElementName = "applicants", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataApplicants Applicants { get; set; }
        [XmlElement(ElementName = "inventors", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataInventors Inventors { get; set; }
    }

    [XmlRoot(ElementName = "invention-title", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataInventiontitle
    {
        [XmlAttribute(AttributeName = "lang")]
        public string Lang { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "patcit", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataPatcit
    {
        [XmlElement(ElementName = "document-id", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataDocumentid> Documentid { get; set; }
        [XmlAttribute(AttributeName = "dnum-type")]
        public string Dnumtype { get; set; }
        [XmlAttribute(AttributeName = "num")]
        public string Num { get; set; }
    }

    [XmlRoot(ElementName = "citation", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataCitation
    {
        [XmlElement(ElementName = "patcit", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataPatcit Patcit { get; set; }
        [XmlElement(ElementName = "category", Namespace = "http://www.epo.org/exchange")]
        public string Category { get; set; }
        [XmlAttribute(AttributeName = "cited-phase")]
        public string Citedphase { get; set; }
        [XmlAttribute(AttributeName = "cited-by")]
        public string Citedby { get; set; }
        [XmlAttribute(AttributeName = "sequence")]
        public string Sequence { get; set; }
    }

    [XmlRoot(ElementName = "references-cited", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataReferencescited
    {
        [XmlElement(ElementName = "citation", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataCitation> Citation { get; set; }
    }

    [XmlRoot(ElementName = "bibliographic-data", Namespace = "http://www.epo.org/exchange")]
    public class PatentBilboDataBibliographicdata
    {
        [XmlElement(ElementName = "publication-reference", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataPublicationreference Publicationreference { get; set; }
        [XmlElement(ElementName = "classification-ipc", Namespace = "http://www.epo.org/exchange")]
        public Classificationipc Classificationipc { get; set; }
        [XmlElement(ElementName = "classifications-ipcr", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataClassificationsipcr Classificationsipcr { get; set; }
        [XmlElement(ElementName = "patent-classifications", Namespace = "http://www.epo.org/exchange")]
        public Patentclassifications Patentclassifications { get; set; }
        [XmlElement(ElementName = "application-reference", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataApplicationreference Applicationreference { get; set; }
        [XmlElement(ElementName = "priority-claims", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataPriorityclaims Priorityclaims { get; set; }
        [XmlElement(ElementName = "parties", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataParties Parties { get; set; }
        [XmlElement(ElementName = "invention-title", Namespace = "http://www.epo.org/exchange")]
        public List<PatentBilboDataInventiontitle> Inventiontitle { get; set; }
        [XmlElement(ElementName = "references-cited", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataReferencescited Referencescited { get; set; }
    }

    [XmlRoot(ElementName = "abstract", Namespace = "http://www.epo.org/exchange")]
    public class Abstract
    {
        [XmlElement(ElementName = "p", Namespace = "http://www.epo.org/exchange")]
        public string P { get; set; }
        [XmlAttribute(AttributeName = "lang")]
        public string Lang { get; set; }
    }

    [XmlRoot(ElementName = "exchange-document", Namespace = "http://www.epo.org/exchange")]
    public class Exchangedocument
    {
        [XmlElement(ElementName = "bibliographic-data", Namespace = "http://www.epo.org/exchange")]
        public PatentBilboDataBibliographicdata Bibliographicdata { get; set; }
        [XmlElement(ElementName = "abstract", Namespace = "http://www.epo.org/exchange")]
        public Abstract Abstract { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "family-id")]
        public string Familyid { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "doc-number")]
        public string Docnumber { get; set; }
        [XmlAttribute(AttributeName = "kind")]
        public string Kind { get; set; }
    }

    [XmlRoot(ElementName = "exchange-documents", Namespace = "http://www.epo.org/exchange")]
    public class Exchangedocuments
    {
        [XmlElement(ElementName = "exchange-document", Namespace = "http://www.epo.org/exchange")]
        public List<Exchangedocument> Exchangedocument { get; set; }
    }

    [XmlRoot(ElementName = "world-patent-data", Namespace = "http://ops.epo.org")]
    public class PatentBilboDataWorldpatentdata
    {
        [XmlElement(ElementName = "meta", Namespace = "http://ops.epo.org")]
        public PatentBilboDataMeta Meta { get; set; }
        [XmlElement(ElementName = "exchange-documents", Namespace = "http://www.epo.org/exchange")]
        public Exchangedocuments Exchangedocuments { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "ops", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ops { get; set; }
        [XmlAttribute(AttributeName = "xlink", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xlink { get; set; }
    }
}
