﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ops.epo.org/3.1/rest-services/published-data/publication/epodoc/dk0554438T3/biblio");
            request.Method = "GET";
            //request.Headers.Add(HttpRequestHeader.Authorization, $"Bearer {token.access_token}");

            //ASCIIEncoding encoding = new ASCIIEncoding();
            //byte[] data = encoding.GetBytes("DK.0554438T3");
            //request.ContentLength = data.Length;

            //Stream newStream = request.GetRequestStream();
            //newStream.Write(data, 0, data.Length);
            //newStream.Close();

            XmlSerializer serializer = new XmlSerializer(typeof(PatentBilboDataWorldpatentdata));
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var result = (PatentBilboDataWorldpatentdata)serializer.Deserialize(reader);
                reader.Close();
                dataStream.Close();

            }
        }
    }
}
